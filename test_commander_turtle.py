from commander_turtle import CommanderTurtle


class TestCommanderTurle:

    def test_get_commands_basic(self):
        o_commander_turtle = CommanderTurtle()
        s_command = "A10"
        a_commands = o_commander_turtle.get_commands(s_command)

        assert len(a_commands) == 1
        assert a_commands[0] == "A10"

    def test_get_commands_empty(self):
        o_commander_turtle = CommanderTurtle()
        s_command = ""
        a_commands = o_commander_turtle.get_commands(s_command)

        assert len(a_commands) == 0

    def test_get_commands_loop(self):
        o_commander_turtle = CommanderTurtle()
        s_command = "L4,C,A50,R90"
        a_commands = o_commander_turtle.get_commands(s_command)

        assert len(a_commands) == 12
        assert a_commands[0] == "C"
        assert a_commands[1] == "A50"
        assert a_commands[2] == "R90"
        assert a_commands[3] == "C"
