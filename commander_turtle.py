from turtle import *
from random import randint


class CommanderTurtle:

    s_VERSION = "0.2"

    def __init__(self):
        self.i_pen_thickness = 1

    def print_help(self):
        """
        Prints help
        :return:
        """
        s_title = "Commander Turtle v. " + self.s_VERSION
        print(s_title)
        print("=" * len(s_title))
        print()
        print("Input your commands / Introdueix les teves comandes:")
        print("A10 or A199 - Advance 10 points, 199... / Avança 10 punts, 199...")
        print("R45 or R97  - Rotate 45, 97... (1 to 360 degrees) / Rota 45 graus, 97... (1 a 360 graus)")
        print("L2 or L50   - Loop the next commands / Repeteix les següents comandes")
        print("C           - Random color / Color aleatori")
        print("CBLACK      - Color Black / Color negre")
        print("CWHITE      - Color White / Color blanc")
        print("CBLUE       - Color Blue / Color blau")
        print("CCYAN       - Color Light Blue (Cyan) / Color blau clar")
        print("CGREEN      - Color Green / Color verd")
        print("CMAGENTA    - Color Magenta / Color magenta (fúcsia)")
        print("CRED        - Color Red / Color vermell")
        print("CYELLOW     - Color Yellow / Color groc")
        print("P10 or P22  - Set Pen thickness to 10, to 22... / Gruix del llapis a 10, o 22...")
        print("P           - Increase the thickness of the pen in 1 / Incrementa el gruix del llapis en 1")
        print("N           - New screen, clear / Neteja la pantalla")
        print("Q - Quit / Sortir")
        print()
        print("Try/Intenta: ")
        print("L360,C,A1,R1")
        print("L4,C,A400,R90")
        print("L100,C,R91,A200")
        print("C,R270,A150,R90,A20,R90,A55,R270,A50,R270,A55,R90,A20,R90,A150,R90,A20,R90,A55,R270,A50,R270,A55,R90,A20,N,C,R270,A150,R90,A90,R90,A20,R90,A70,R270,A45,R270,A50,R90,A20,R90,A50,R270,A45,R270,A70,R90,A20,R90,A90,N,C,R270,A150,R90,A20,R90,A130,R270,A70,R90,A20,R90,A90,N,C,R270,A150,R90,A20,R90,A130,R270,A70,R90,A20,R90,A90,N,C,R270,A150,R90,A90,R90,A150,R90,A90")
        print()

    def ask_commands_from_keyboard(self):
        """
        Ask the user to enter the commands
        :return: String with the commands
        """

        while True:
            s_command = input(":")
            if s_command == "":
                print("Please enter a command / Si us plau introdueix una comanda")
                continue
            break

        return s_command

    def get_commands(self, s_command):
        """
        Breaks a command or sequence of commands in an array of commands
        :return:
        """
        # Asks user to input a command
        a_commands = s_command.split(",")

        a_commands = self.remove_empty_commands(a_commands)

        if len(a_commands) < 1:
            return []

        s_first_command = a_commands[0]
        if len(s_first_command) > 0:
            if s_first_command[0] == "L":
                # Capture the number of times we want to loop
                i_times_to_repeat = 1
                if len(s_first_command) > 1:
                    i_times_to_repeat = int(s_first_command[1:])

                a_commands.remove(s_first_command)
                a_commands_new = a_commands * i_times_to_repeat

                a_commands = a_commands_new

        return a_commands

    def remove_empty_commands(self, a_commands):
        """
        Removes any empty command
        :param a_commands:
        :return:
        """

        i_pointer = 0
        while i_pointer < len(a_commands):
            s_command = a_commands[i_pointer]
            s_command = s_command.strip()

            if len(s_command) == 0:
                del a_commands[i_pointer]
                # start again
                i_pointer = 0
                continue

            i_pointer = i_pointer + 1

        return a_commands

    def main(self):
        self.print_help()
        bgcolor("black")
        i_r = randint(0, 255)
        i_g = randint(0, 255)
        i_b = randint(0, 255)
        colormode(255)
        pencolor(i_r, i_g, i_b)
        speed(0)

        b_exit = False
        while b_exit is False:
            s_commands = o_commander_turtle.ask_commands_from_keyboard()
            a_commands = o_commander_turtle.get_commands(s_commands)

            for s_single_command in a_commands:

                s_single_command = s_single_command.upper()
                if s_single_command[0] == "A":
                    s_avanca = s_single_command[1:]
                    i_avanca = int(s_avanca) + 1

                    fd(i_avanca)

                if s_single_command[0] == "R":
                    if len(s_single_command) > 1:
                        s_rota = s_single_command[1:]
                    else:
                        s_rota = 90
                    i_rota = int(s_rota)

                    rt(i_rota)

                if s_single_command[0] == "C":
                    if len(s_single_command) == 1:
                        # Random color
                        i_r = randint(0, 255)
                        i_g = randint(0, 255)
                        i_b = randint(0, 255)

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                    if s_single_command == "CRED":
                        i_r = 255
                        i_g = 0
                        i_b = 0

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                    if s_single_command == "CBLUE":
                        i_r = 0
                        i_g = 0
                        i_b = 255

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                    if s_single_command == "CGREEN":
                        i_r = 0
                        i_g = 255
                        i_b = 0

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                    if s_single_command == "CYELLOW":
                        i_r = 255
                        i_g = 255
                        i_b = 0

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                    if s_single_command == "CMAGENTA":
                        i_r = 255
                        i_g = 0
                        i_b = 255

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                    if s_single_command == "CCYAN":
                        i_r = 0
                        i_g = 255
                        i_b = 255

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                    if s_single_command == "CBLACK":
                        i_r = 0
                        i_g = 0
                        i_b = 0

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                    if s_single_command == "CWHITE":
                        i_r = 255
                        i_g = 255
                        i_b = 255

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                if s_single_command[0] == "P":
                    if len(s_single_command) == 1:
                        self.i_pen_thickness += 1
                    else:
                        self.i_pen_thickness = int(s_single_command[1:])

                    pensize(self.i_pen_thickness)

                if s_single_command[0] == "N":
                    clear()
                    reset()

                if s_single_command[0] == "Q":
                    b_exit = True
                    break

        print("Press on the image to close it")
        exitonclick()


if __name__ == "__main__":

    o_commander_turtle = CommanderTurtle()
    o_commander_turtle.main()
