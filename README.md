# Commander Turtle

Version 0.2.1

http://www.commanderturtle.com

https://blog.carlesmateo.com/commander-turtle/

![](img/2022-07-22-commanderturtle-hello.png)

## Getting started

This is an Open Source project created by Carles Mateo on 2022-07-21.

It provides a way for children to create simple programs that draw on the screen, and the ability to share these programs with others.

Please, visit https://blog.carlesmateo.com/commander-turtle/ for documentation, videos, and examples.

Launch the program with:

```python commander_turtle.py```

Type your commands on the text interface:

![](img/2022-07-21-circle-with-increasing-pen-thickness-cut.png)
